﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid.Domain.Model
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string Shortcut { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Continent Continent { get; set; }
        public int ContinentId { get; set; }
        
    }
}
