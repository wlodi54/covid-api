﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid.Domain.Model
{
    public class DailyMeasurment
    {
        public DateTime DateMeasurment { get; set; }
        public DateTime DateInsertData { get; set; }
        public string FileName { get; set; }
        public Guid Id { get; set; }

    }
}
