﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid.Domain.Model
{
    public class CountryAlias
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
    }
}
