﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid.Domain.Model
{
    public class Continent
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Shortcut { get; set; }
    }
}
