﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Covid.Domain.Model
{
    public class DailyMeasurmentValues
    {
        public int Id { get; set; }
        public string State { get; set; }
        public string LastUpdate { get; set; }
        public string Confirmed { get; set; }
        public string  Deaths { get; set; }
        public string Recovered { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Country Country { get; set; }
        public Guid CountryId { get; set; }
        public DailyMeasurment DailyMeasurment { get; set; }
        public Guid MeasurmentId { get; set; }

    }
}
